import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class Main {

    public static int countOne = 0;
    public static int countTwo = 0;
    public static int countTree = 0;
    public static int countFore = 0;
    public static int countFive = 0;
    public static int countSix = 0;
    public static int countSeven = 0;
    public static int countEight = 0;
    public static int countNine = 0;
    public static int countTen = 0;
    public static int countEleven = 0;
    public static int countTwelve = 0;

    public static void main(String[] args) throws Exception {

        List<Employees> employees = Arrays.asList(
                new Employees("Max", "Taylor", 31, 11, 1999),
                new Employees("Ivan", "Ivanov", 11, 1, 2021),
                new Employees("Vadim", "Ivanov", 4, 2, 1999),
                new Employees("Ivan", "Harris", 2, 1, 1983),
                new Employees("Lina", "Smith", 11, 11, 1899),
                new Employees("Bogdan", "Johnson", 12, 11, 2011),
                new Employees("Viliam", "Jones", 29, 12, 2005),
                new Employees("Jon", "Brown", 8, 6, 2000),
                new Employees("Max", "Miller", 6, 8, 1999),
                new Employees("Max", "Wilson", 9, 9, 1998)
        );
        System.out.println("Каличество сотрудников = " + counterEmployees(employees));
        System.out.println("Mесяц № " + determineMonth(counterEmployees(employees)));
    }

    public static int counterEmployees(List<Employees> employees) {

        for (Employees employee : employees) {
            switch (employee.getMonth()) {
                case 1:
                    countOne++;
                    break;
                case 2:
                    countTwo++;
                    break;
                case 3:
                    countTree++;
                    break;
                case 4:
                    countFore++;
                    break;
                case 5:
                    countFive++;
                    break;
                case 6:
                    countSix++;
                    break;
                case 7:
                    countSeven++;
                    break;
                case 8:
                    countEight++;
                    break;
                case 9:
                    countNine++;
                    break;
                case 10:
                    countTen++;
                    break;
                case 11:
                    countEleven++;
                    break;
                case 12:
                    countTwelve++;
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + employee.getMonth());
            }
        }
        return mostPopularMonth(countOne, countTwo, countTree, countFore, countFive, countSix, countSeven, countEight, countNine, countTen, countEleven, countTwelve);
    }

    public static int mostPopularMonth(int countOne, int countTwo, int countTree, int countFore, int countFive, int countSix, int countSeven, int countEight, int countNine, int countTen, int countEleven, int countTwelve) {
        return Stream.of(countOne, countTwo, countTree, countFore, countFive, countSix, countSeven, countEight, countNine, countTen, countEleven, countTwelve)
                .max(Comparator.naturalOrder())
                .get();
    }

    public static int determineMonth(int value) {
        if (countOne == value) {
            return 1;
        } else if (countTwo == value) {
            return 2;
        } else if (countTree == value) {
            return 3;
        } else if (countFore == value) {
            return 4;
        } else if (countFive == value) {
            return 5;
        } else if (countSix == value) {
            return 6;
        } else if (countSeven == value) {
            return 7;
        } else if (countEight == value) {
            return 8;
        } else if (countNine == value) {
            return 9;
        } else if (countTen == value) {
            return 10;
        } else if (countEleven == value) {
            return 11;
        } else if (countTwelve == value) {
            return 12;
        } else return 0;
    }
}
