public class Employees {

    private String name;
    private String surname;
    private int day;
    private int month;
    private int year;

    public Employees(String name, String surname, int day, int month, int year) throws Exception {
        this.name = name;
        this.surname = surname;

        if (day >= 0 && day < 32) {
            this.day = day;
        } else throw new Exception();

        if (month >= 0 && month < 13) {
            this.month = month;
        } else throw new Exception();

        if (year >= 0 && year < 2022) {
            this.year = year;
        } else throw new Exception();

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
